#include "config.hpp"
#include "scope_guard.hpp"
#include "service.hpp"
#include "shutdown_server.hpp"
#include "version.hpp"
#include <agrpc/asio_grpc.hpp>
#include <agrpc/grpc_context.hpp>
#include <asio/execution/outstanding_work.hpp>
#include <asio/io_context.hpp>
#include <glaze/json/prettify.hpp>
#include <glaze/json/read.hpp>
#include <grpcpp/grpcpp.h>
#include <grpcpp/server_context.h>
#include <grpcpp/support/status.h>
#include <iostream>
#include <quill/Quill.h>
#include <quill/Utility.h>
#include <quill/detail/LogMacros.h>


int main(int argc, char const* argv[])
{
	try
	{
		const auto config_file_path =
			argc >= 2 ? argv[1] : "operator_config.json";
		std::shared_ptr<quill::Handler> handler =
			quill::stdout_handler(); /** for stdout **/
		handler->set_pattern("%(ascii_time) [%(thread)] %(fileline:<28) "
							 "LOG_%(level_name) %(message)");

		quill::Config cfg;
		cfg.enable_console_colours = true;
		cfg.default_handlers.push_back(handler);

		quill::configure(cfg);
		quill::start();
		LOG_DEBUG(quill::get_logger(), "Logging started");
		Config config(config_file_path);

		LOG_INFO(quill::get_logger(), "Config: {}",
				 quill::utility::to_string(config));


		OperatorServiceImpl service(
			std::format("{}:{}", config.server_address, config.server_port),
			config.rmin, config.rmax, std::format("{}:{}", config.service_address, config.service_port));
		std::unique_ptr<grpc::Server> server;

		grpc::ServerBuilder builder;
		agrpc::GrpcContext grpc_context{builder.AddCompletionQueue()};
		builder.AddListeningPort(config.service_address.append(
									 std::format(":{}", config.service_port)),
								 grpc::InsecureServerCredentials());
		builder.RegisterService(&service);
		server = builder.BuildAndStart();
		if (!bool{server})
		{
			LOG_CRITICAL(quill::get_logger(),
						 "GRPC server not created! Exiing....");
			return 1;
		}
		ServerShutdown shutdown{*server, grpc_context};
		asio::io_context io_context{1};
		std::optional guard{
			asio::require(io_context.get_executor(),
						  asio::execution::outstanding_work_t::tracked)};
		std::thread io_context_thread{&run_io_context, std::ref(io_context)};
		ScopeGuard on_exit{[&]
						   {
							   guard.reset();
							   io_context_thread.join();
						   }};
		grpc_context.run();
	}
	catch (const std::exception& e)
	{
		LOG_CRITICAL(quill::get_logger(), "Exception: {}", e.what());
		return 1;
	}
	return 0;
}
