#include "service.hpp"
#include "time.hpp"
#include <agrpc/asio_grpc.hpp>
#include <agrpc/detail/forward.hpp>
#include <asio/co_spawn.hpp>
#include <asio/detached.hpp>
#include <asio/use_awaitable.hpp>
#include <chrono>
#include <google/protobuf/duration.pb.h>
#include <google/protobuf/empty.pb.h>
#include <google/protobuf/timestamp.pb.h>
#include <google/protobuf/util/time_util.h>
#include <grpc/server/v1/server.pb.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <quill/Quill.h>
#include <quill/detail/LogMacros.h>
#include <random>
#include <thread>
#include <uuid.h>

#include "grpc/server/v1/server.grpc.pb.h"

grpc::Status OperatorServiceImpl::Check(grpc::ServerContext* context,
										const OperatorStatusRequest* request,
										OperatorStatusResponse* response)
{
	LOG_INFO(quill::get_logger(), "Got Status check request");
	uuids::uuid request_id =
		uuids::uuid::from_string(request->operatorid()).value();
	if (request_id != id_)
	{
		grpc::Status rstatus(grpc::StatusCode::INVALID_ARGUMENT,
							 "Wrong operator id");
		LOG_ERROR(quill::get_logger(),
				  "Server requested status check with wrong id");
		LOG_DEBUG(quill::get_logger(), "Server id{}!=current{}",
				  request->operatorid(), uuids::to_string(id_));
		return rstatus;
	}
	switch (status_)
	{
		case Status::Wait:
			response->set_status(
				grpc::operatorser::v1::OperatorStatusResponse_WorkStatus::
					OperatorStatusResponse_WorkStatus_Wait);
			break;
		case Status::Busy:
			response->set_status(
				grpc::operatorser::v1::OperatorStatusResponse_WorkStatus::
					OperatorStatusResponse_WorkStatus_Busy);
			break;
	}
	LOG_INFO(quill::get_logger(), "Responded with status: {}",
			 (int)response->status());

	return grpc::Status::OK;
}

grpc::Status OperatorServiceImpl::Call(grpc::ServerContext* context,
									   const CallRequest* request,
									   CallResponse* response)
{
    grpc::Status rstatus;
    uuids::uuid request_id =
        uuids::uuid::from_string(request->operatorid()).value();
    if (request_id != id_)
    {
        rstatus = grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                             "Wrong operator id");
        LOG_ERROR(quill::get_logger(),
                  "Server requested status check with wrong id");
        LOG_DEBUG(quill::get_logger(), "Server id{}!=current{}",
                  request->operatorid(), uuids::to_string(id_));
        return rstatus;
    }
    status_ = Status::Busy;
    LOG_INFO(quill::get_logger(), "Got call, setting status to BUSY");

    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<int> distribution(rmin_.count(), rmax_.count());
    auto seconds = distribution(generator);

    auto duration = std::chrono::seconds(seconds);
    LOG_DEBUG(quill::get_logger(), "Duration: {}\nRange: [{},{}]", duration,
              rmin_, rmax_);
    auto start = std::chrono::high_resolution_clock::now();
    LOG_INFO(quill::get_logger(), "Duration: {}", formatDuration(duration));
    std::this_thread::sleep_for(duration);
    auto stop = std::chrono::high_resolution_clock::now();


    LOG_DEBUG(quill::get_logger(),
              "Call info:\nCallId: {}\nStart: {}\nEnd: {}\nDuration:{}",
              request->callid(),
              formatTimePoint(start),
              formatTimePoint(stop), formatDuration(duration));

    response->set_callid(request->callid());
    response->set_starttime(formatTimePoint(start));
    response->set_endtime(formatTimePoint(stop));
    LOG_INFO(quill::get_logger(), "Call ended");
    LOG_INFO(quill::get_logger(), "Time: {}", duration.count());
    status_ = Status::Wait;
    LOG_INFO(quill::get_logger(), "End of call, setting status to WAIT");
    rstatus = grpc::Status::OK;
	return rstatus;
}

OperatorServiceImpl::OperatorServiceImpl(std::string server_address,
										 std::uint_fast64_t rmin,
										 std::uint_fast64_t rmax,
										 std::string exchange_info)
	: rmin_(rmin)
	, rmax_(rmax)
	, server_address_(server_address)
	, exchange_info_(exchange_info)
{
	grpc::Status rstatus;
	LOG_INFO(quill::get_logger(), "Server address: {}", server_address);
	grpc::server::v1::Server::Stub stub{grpc::CreateChannel(
		server_address, grpc::InsecureChannelCredentials())};
	agrpc::GrpcContext grpc_context;
	asio::co_spawn(
		grpc_context,
		[&]() -> asio::awaitable<void>
		{
			LOG_INFO(quill::get_logger(), "Requesting id from server");
			using RPC =
				agrpc::ClientRPC<&grpc::server::v1::Server::Stub::
									 PrepareAsyncOperatorInitialization>;
			grpc::ClientContext client_context;
			client_context.set_deadline(std::chrono::system_clock::now() +
										std::chrono::seconds(5));
			grpc::server::v1::OperatorInitializationResponse response;
			grpc::server::v1::OperatorInitializationRequest request;
			request.set_exchangeinfo(exchange_info_);
			rstatus =
				co_await RPC::request(grpc_context, stub, client_context,
									  request, response, asio::use_awaitable);
			LOG_DEBUG(quill::get_logger(), "Status: {}", rstatus.ok());
			LOG_INFO(quill::get_logger(), "Got Id from Server");
			this->id_ = uuids::uuid::from_string(response.newid()).value();
			LOG_DEBUG(quill::get_logger(), "Assigned ID: {}", response.newid());
			LOG_INFO(quill::get_logger(), "Initialization complete");
			co_return;
		},
		asio::detached);

	grpc_context.run();
}

OperatorServiceImpl::~OperatorServiceImpl()
{
	grpc::Status rstatus;

	grpc::server::v1::Server::Stub stub{grpc::CreateChannel(
		server_address_, grpc::InsecureChannelCredentials())};
	agrpc::GrpcContext grpc_context;

	LOG_INFO(quill::get_logger(), "Service Destruction");
	asio::co_spawn(
		grpc_context,
		[&]() -> asio::awaitable<void>
		{
			LOG_INFO(quill::get_logger(),
					 "Requesting server to forget about operator");
			using RPC = asio::use_awaitable_t<>::as_default_on_t<
				agrpc::ClientRPC<&grpc::server::v1::Server::Stub::
									 PrepareAsyncOperatorDestruction>>;
			grpc::ClientContext client_context;
			client_context.set_wait_for_ready(true);
			grpc::server::v1::OperatorDestructionRequest request;
			google::protobuf::Empty response;
			request.set_operatorid(uuids::to_string(this->id_));
			rstatus = co_await RPC::request(grpc_context, stub, client_context,
											request, response);

			LOG_DEBUG(quill::get_logger(), "Status: {}", rstatus.ok());
			LOG_INFO(quill::get_logger(), "Destruction complete");
			co_return;
		},
		asio::detached);

	grpc_context.run();
}
