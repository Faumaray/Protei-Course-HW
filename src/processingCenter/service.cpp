#include "service.hpp"
#include "time.hpp"
#include <agrpc/asio_grpc.hpp>
#include <agrpc/detail/forward.hpp>
#include <agrpc/grpc_context.hpp>
#include <asio/co_spawn.hpp>
#include <asio/detached.hpp>
#include <asio/use_awaitable.hpp>
#include <cstdint>
#include <grpc/operatorservice/v1/operator.grpc.pb.h>
#include <grpc/operatorservice/v1/operator.pb.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <regex>


#include <chrono>
#include <quill/Quill.h>
#include <quill/detail/LogMacros.h>
#include <uuid.h>

std::string ServerServiceImpl::extractIpAddress(const std::string& input)
{
	std::ostringstream out;
	for (std::size_t i = 0; i < input.length(); ++i)
	{
		if (input[i] == '%' && i + 2 < input.length())
		{
			int hexValue;
			std::istringstream hexStream(input.substr(i + 1, 2));
			hexStream >> std::hex >> hexValue;
			out << static_cast<char>(hexValue);
			i += 2;
		}
		else if (i + 5 < input.length() &&
				 (input.substr(i, 5) == "ipv6:" ||
				  input.substr(i, 5) == "ipv4:"))
		{
			i += 4;
		}
		else
		{
			out << input[i];
		}
	}
	std::string result_string = out.str();

	result_string.erase(
		std::remove(result_string.begin(), result_string.end(), '['),
		result_string.end());
	result_string.erase(
		std::remove(result_string.begin(), result_string.end(), ']'),
		result_string.end());
	return result_string;
}

grpc::Status ServerServiceImpl::OperatorInitialization(
	grpc::ServerContext* context, const OperatorInitializationRequest* request,
	OperatorInitializationResponse* response)
{

	LOG_INFO(quill::get_logger(), "Initizing new operaor");
	std::random_device rd;
	auto seed_data = std::array<int, std::mt19937::state_size>{};
	std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
	std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
	std::mt19937 generator(seq);
	uuids::uuid_random_generator gen{generator};
	uuids::uuid newid = gen();

	if (operators_.find(newid) != operators_.end())
	{
		LOG_DEBUG(quill::get_logger(), "UUID already exists, creaing new");
		newid = gen();
	}

	response->set_newid(uuids::to_string(newid));
	std::string address	 = extractIpAddress(context->peer());
	std::size_t colonPos = address.find_last_of(':');
	operators_.emplace(newid, request->exchangeinfo());
	LOG_INFO(quill::get_logger(),
			 "Adding new operator to local storage with id = {}",
			 uuids::to_string(newid));
	LOG_DEBUG(quill::get_logger(), "New operator info: {}", operators_[newid]);
	return grpc::Status::OK;
}

grpc::Status ServerServiceImpl::OperatorDestruction(
	grpc::ServerContext* context, const OperatorDestructionRequest* request,
	google::protobuf::Empty* response)
{
	uuids::uuid request_id =
		uuids::uuid::from_string(request->operatorid()).value();
	if (operators_.find(request_id) == operators_.end())
	{
		LOG_WARNING(quill::get_logger(),
					"Not found operator id in local storage");
		return grpc::Status(grpc::StatusCode::NOT_FOUND,
							"Not found operaor in local storage");
	}
	LOG_INFO(quill::get_logger(), "Forgetting about operator with id: {}",
			 request->operatorid());
	LOG_DEBUG(quill::get_logger(), "Removing operator:\nid = {}\naddress = {}",
			  request->operatorid(), operators_[request_id]);
	operators_.erase(request_id);
	return grpc::Status::OK;
}

uuids::uuid ServerServiceImpl::get_first_free()
{
	for (const auto& [id, address] : operators_)
	{
		grpc::Status rstatus;
		LOG_INFO(quill::get_logger(), "Server address: {}", address);
		grpc::operatorser::v1::OperatorService::Stub stub{
			grpc::CreateChannel(address, grpc::InsecureChannelCredentials())};
		agrpc::GrpcContext grpc_context;
		grpc::operatorser::v1::OperatorStatusResponse_WorkStatus status;
		asio::co_spawn(
			grpc_context,
			[&]() -> asio::awaitable<void>
			{
				LOG_INFO(quill::get_logger(), "Requesting status");
				using RPC =
					agrpc::ClientRPC<&grpc::operatorser::v1::OperatorService::
										 Stub::PrepareAsyncCheck>;
				grpc::ClientContext client_context;
				client_context.set_deadline(std::chrono::system_clock::now() +
											std::chrono::seconds(5));
				grpc::operatorser::v1::OperatorStatusResponse response;
				grpc::operatorser::v1::OperatorStatusRequest request;

				request.set_operatorid(uuids::to_string(id));
				rstatus = co_await RPC::request(grpc_context, stub,
												client_context, request,
												response, asio::use_awaitable);
				LOG_DEBUG(quill::get_logger(), "Status: {}", rstatus.ok());
				LOG_INFO(quill::get_logger(), "Got Status from Operator");
				status = response.status();
				co_return;
			},
			asio::detached);

		grpc_context.run();

		if (status ==
			grpc::operatorser::v1::OperatorStatusResponse_WorkStatus::
				OperatorStatusResponse_WorkStatus_Wait)
			return id;
	}

	return uuids::uuid();
}

void ServerServiceImpl::schedule_call(uuids::uuid operatorId,
									  CallRecord& callInfo)
{
	grpc::Status rstatus;
	LOG_INFO(quill::get_logger(), "Server address: {}", operators_[operatorId]);
	grpc::operatorser::v1::OperatorService::Stub stub{grpc::CreateChannel(
		operators_[operatorId], grpc::InsecureChannelCredentials())};
	agrpc::GrpcContext grpc_context;
	asio::co_spawn(
		grpc_context,
		[&]() -> asio::awaitable<void>
		{
			LOG_INFO(quill::get_logger(), "Requesting call");
			using RPC =
				agrpc::ClientRPC<&grpc::operatorser::v1::OperatorService::Stub::
									 PrepareAsyncCall>;
			grpc::ClientContext client_context;
			grpc::operatorser::v1::CallResponse response;
			grpc::operatorser::v1::CallRequest request;
            client_context.set_wait_for_ready(true);
            client_context.set_deadline(std::chrono::high_resolution_clock::now() + std::chrono::hours(120));

			request.set_operatorid(uuids::to_string(operatorId));
			request.set_callid(uuids::to_string(callInfo.id));
    
			rstatus =
				co_await RPC::request(grpc_context, stub, client_context,
									  request, response, asio::use_awaitable);
			LOG_DEBUG(quill::get_logger(), "Status: {}", rstatus.ok());

			callInfo.operator_id = operatorId;
			callInfo.end_time =
				parseTimePoint<std::chrono::high_resolution_clock,
							   std::chrono::milliseconds>(response.endtime());
			callInfo.operator_answer_time =
				parseTimePoint<std::chrono::high_resolution_clock,
							   std::chrono::milliseconds>(response.starttime());
			callInfo.status = "Ok";
			callInfo.write_cdr();
			co_return;
		},
		asio::detached);

	grpc_context.run();
}
