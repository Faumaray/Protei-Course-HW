#include "request_parser.hpp"
#include "request.hpp"
#include <algorithm>
#include <cctype>
#include <optional>
#include <sstream>
#include <string>

#include <asio/yield.hpp>

std::string request_parser::content_length_name_ = "Content-Length";

std::optional<bool> request_parser::consume(request& req, char c)
{
	reenter(this)
	{
		req.method.clear();
		req.uri.clear();
		req.http_version_major = 0;
		req.http_version_minor = 0;
		req.headers.clear();
		req.content.clear();
		content_length_ = 0;

		while (is_char(c) && !is_ctl(c) && !is_tspecial(c) && c != ' ')
		{
			req.method.push_back(c);
			yield return std::optional<bool>();
		}
		if (req.method.empty())
			return false;

		if (c != ' ')
			return false;
		yield return std::optional<bool>();

		while (!is_ctl(c) && c != ' ')
		{
			req.uri.push_back(c);
			yield return std::optional<bool>();
		}
		if (req.uri.empty())
			return false;

		if (c != ' ')
			return false;
		yield return std::optional<bool>();

		if (c != 'H')
			return false;
		yield return std::optional<bool>();
		if (c != 'T')
			return false;
		yield return std::optional<bool>();
		if (c != 'T')
			return false;
		yield return std::optional<bool>();
		if (c != 'P')
			return false;
		yield return std::optional<bool>();

		if (c != '/')
			return false;
		yield return std::optional<bool>();

		if (!is_digit(c))
			return false;
		while (is_digit(c))
		{
			req.http_version_major = req.http_version_major * 10 + c - '0';
			yield return std::optional<bool>();
		}

		if (c != '.')
			return false;
		yield return std::optional<bool>();

		if (!is_digit(c))
			return false;
		while (is_digit(c))
		{
			req.http_version_minor = req.http_version_minor * 10 + c - '0';
			yield return std::optional<bool>();
		}

		if (c != '\r')
			return false;
		yield return std::optional<bool>();
		if (c != '\n')
			return false;
		yield return std::optional<bool>();

		while ((is_char(c) && !is_ctl(c) && !is_tspecial(c) && c != '\r') ||
			   (c == ' ' || c == '\t'))
		{
			if (c == ' ' || c == '\t')
			{
				if (req.headers.empty())
					return false;
				while (c == ' ' || c == '\t')
					yield return std::optional<bool>();
			}
			else
			{
				req.headers.push_back(header());

				while (is_char(c) && !is_ctl(c) && !is_tspecial(c) && c != ':')
				{
					req.headers.back().name.push_back(c);
					yield return std::optional<bool>();
				}

				if (c != ':')
					return false;
				yield return std::optional<bool>();
				if (c != ' ')
					return false;
				yield return std::optional<bool>();
			}

			while (is_char(c) && !is_ctl(c) && c != '\r')
			{
				req.headers.back().value.push_back(c);
				yield return std::optional<bool>();
			}

			if (c != '\r')
				return false;
			yield return std::optional<bool>();
			if (c != '\n')
				return false;
			yield return std::optional<bool>();
		}

		if (c != '\r')
			return false;
		yield return std::optional<bool>();
		if (c != '\n')
			return false;

		for (std::size_t i = 0; i < req.headers.size(); ++i)
		{
			if (headers_equal(req.headers[i].name, content_length_name_))
			{
				std::stringstream ss(req.headers[i].value);
				ss >> content_length_;
				if (ss.fail())
					return false;
			}
		}

		// Content.
		while (req.content.size() < content_length_)
		{
			yield return std::optional<bool>();
			req.content.push_back(c);
		}
	}

	return true;
}

#include <asio/unyield.hpp>

bool request_parser::is_char(int c)
{
	return c >= 0 && c <= 127;
}

bool request_parser::is_ctl(int c)
{
	return (c >= 0 && c <= 31) || (c == 127);
}

bool request_parser::is_tspecial(int c)
{
	switch (c)
	{
		case '(':
		case ')':
		case '<':
		case '>':
		case '@':
		case ',':
		case ';':
		case ':':
		case '\\':
		case '"':
		case '/':
		case '[':
		case ']':
		case '?':
		case '=':
		case '{':
		case '}':
		case ' ':
		case '\t':
			return true;
		default:
			return false;
	}
}

bool request_parser::is_digit(int c)
{
	return c >= '0' && c <= '9';
}

bool request_parser::tolower_compare(char a, char b)
{
	return std::tolower(a) == std::tolower(b);
}

bool request_parser::headers_equal(const std::string& a, const std::string& b)
{
	if (a.length() != b.length())
		return false;

	return std::equal(a.begin(), a.end(), b.begin(),
					  &request_parser::tolower_compare);
}
