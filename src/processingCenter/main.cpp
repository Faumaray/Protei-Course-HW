#include "call_handler.hpp"
#include "config.hpp"
#include "scope_guard.hpp"
#include "server.hpp"
#include "service.hpp"
#include "shutdown_server.hpp"
#include "version.hpp"
#include <agrpc/asio_grpc.hpp>
#include <agrpc/grpc_context.hpp>
#include <asio.hpp>
#include <asio/io_context.hpp>
#include <grpc/operatorservice/v1/operator.grpc.pb.h>
#include <grpcpp/security/server_credentials.h>
#include <grpcpp/server_builder.h>
#include <iostream>
#include <quill/LogLevel.h>
#include <quill/Quill.h>
#include <quill/Utility.h>
#include <quill/detail/LogMacros.h>
#include <string>
auto main(int argc, char const* argv[]) -> int
{
	try
	{
		const auto config_file_path =
			argc >= 2 ? argv[1] : "center_config.json";

		std::shared_ptr<quill::Handler> handler =
			quill::stdout_handler(); /** for stdout **/
		handler->set_pattern("%(ascii_time) [%(thread)] %(fileline:<28) "
							 "LOG_%(level_name) %(message)");

		quill::Config cfg;
		cfg.enable_console_colours = true;
		cfg.default_handlers.push_back(handler);

		quill::configure(cfg);
		quill::start();
		quill::get_logger()->set_log_level(quill::LogLevel::TraceL3);
		LOG_INFO(quill::get_logger(), "Logging started");
		Config config(config_file_path);

		LOG_INFO(quill::get_logger(), "Config: {}",
				 quill::utility::to_string(config));


		auto service = std::make_shared<ServerServiceImpl>();
		std::unique_ptr<grpc::Server> grpc_server;

		grpc::ServerBuilder builder;
		agrpc::GrpcContext grpc_context{builder.AddCompletionQueue()};
		builder.AddListeningPort(
			std::format("{}:{}", config.service_address, config.service_port),
			grpc::InsecureServerCredentials());
		builder.RegisterService(service.get());
		grpc_server = builder.BuildAndStart();
		if (!bool{grpc_server})
		{
			LOG_CRITICAL(quill::get_logger(),
						 "GRPC server not created! Exiing....");
			return 1;
		}
		asio::io_context io_context{1};
		auto call_handler = std::make_shared<CallHandler>(
			service, config.queue_capacity, config.rmin, config.rmax);
		server(io_context, config.server_address,
			   std::to_string(config.server_port), call_handler)();
		asio::signal_set signals(io_context);
		signals.add(SIGINT);
		signals.add(SIGTERM);
#if defined(SIGQUIT)
		signals.add(SIGQUIT);
#endif
		signals.async_wait(std::bind(&asio::io_context::stop, &io_context));
        ServerShutdown shutdown{*grpc_server, grpc_context};
		std::optional guard{
			asio::require(io_context.get_executor(),
						  asio::execution::outstanding_work_t::tracked)};
		std::thread io_context_thread{&run_io_context, std::ref(io_context)};
		ScopeGuard on_exit{[&]
						   {
							   guard.reset();
							   io_context_thread.join();
						   }};
		grpc_context.run();
	}
	catch (const std::exception& e)
	{
		LOG_CRITICAL(quill::get_logger(), "Exception: {}", e.what());
		return 1;
	}

	return 0;
}
