#include "server.hpp"
#include "call_handler.hpp"
#include "reply.hpp"
#include "request.hpp"

server::server(asio::io_context& io_context, const std::string& address,
			   const std::string& port, std::shared_ptr<CallHandler> handler)
	: request_handler_(handler)
{
	tcp::resolver resolver(io_context);
	asio::ip::tcp::endpoint endpoint = *resolver.resolve(address, port).begin();
	acceptor_.reset(new tcp::acceptor(io_context, endpoint));
}

#include <asio/yield.hpp>

void server::operator()(asio::error_code ec, std::size_t length)
{
	if (!ec)
	{
		reenter(this)
		{
			do
			{
				socket_.reset(new tcp::socket(acceptor_->get_executor()));

				yield acceptor_->async_accept(*socket_, *this);

				fork server (*this)();

			} while (is_parent());

			buffer_.reset(new std::array<char, 8192>);
			request_.reset(new request);

			do
			{
				yield socket_->async_read_some(asio::buffer(*buffer_), *this);

				std::tie(valid_request_, std::ignore) = request_parser_.parse(
					*request_, buffer_->data(), buffer_->data() + length);

			} while (!valid_request_.has_value());

			reply_.reset(new reply);

			if (valid_request_.value())
			{
				CallHandler* handler = request_handler_.get();
				handler->operator()(*request_, *reply_);
			}
			else
			{
				*reply_ = reply::stock_reply(reply::bad_request);
			}

			yield asio::async_write(*socket_, reply_->to_buffers(), *this);

			socket_->shutdown(tcp::socket::shutdown_both, ec);
		}
	}
}

#include <asio/unyield.hpp>
