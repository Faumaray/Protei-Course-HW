#include "call_handler.hpp"
#include "call_record.hpp"
#include <algorithm>
#include <asio/awaitable.hpp>
#include <asio/co_spawn.hpp>
#include <asio/detached.hpp>
#include <chrono>
#include <glaze/json/json_ptr.hpp>
#include <glaze/json/read.hpp>
#include <quill/Quill.h>
#include <quill/detail/LogMacros.h>
#include <string>
#include <thread>
#include <uuid.h>
#include <vector>

CallHandler::CallHandler(std::shared_ptr<ServerServiceImpl> service,
						 std::size_t capacity, std::uint_fast32_t rmin,
						 std::uint_fast32_t rmax)
	: capacity_(capacity)
	, service_(service)
	, queue_({})
	, rmin_(rmin)
	, rmax_(rmax)
{
}

void CallHandler::start_checker()
{
	std::thread(
		[&]()
		{
			while (true)
			{
				{
					std::uint_fast32_t timeout_counter = 0;
					for (auto i = 0; i < queue_.size(); i++)
					{
						if (queue_[i].timeout_point <=
							std::chrono::high_resolution_clock::now())
						{
							queue_[i].call.end_time =
								std::chrono::high_resolution_clock::now();
							queue_[i].call.status = "timeout";
							queue_[i].call.write_cdr();
							queue_.erase(queue_.begin() + i - timeout_counter);
							timeout_counter++;
						}
					}
				}
				auto available_operator = this->service_->get_first_free();
				if (available_operator != uuids::uuid())
				{
					CallInfo current_call;
					{
						current_call = queue_.front();
						queue_.pop_front();
					}
					std::thread(
						[&]() {
							this->service_->schedule_call(available_operator,
														  current_call.call);
						})
						.detach();
				}

				std::this_thread::sleep_for(
					std::chrono::duration(std::chrono::seconds(5)));
			}
		})
		.detach();
	checker_started_ = true;
}
void CallHandler::operator()(const request& req, reply& rep)
{
	LOG_INFO(quill::get_logger(), "Incoming request{}", req.content);
	if (!checker_started_)
	{
		start_checker();
	}
	if (req.method != "POST")
	{

		LOG_INFO(quill::get_logger(), "Req method", req.method);
		rep = reply::stock_reply(reply::not_implemented);
		return;
	}
	auto from_json = glz::get_as_json<std::string, "/number">(req.content);
	if (req.content.empty() || from_json->empty())
	{
		LOG_INFO(quill::get_logger(), "Empty or not valied request");
		rep = reply::stock_reply(reply::no_content);
		return;
	}
	std::string number(from_json->data());
	bool inQueue = false;
	LOG_INFO(quill::get_logger(), "Queue size: {}", queue_.size());

	for (auto call_in_queue : queue_)
	{
		LOG_DEBUG(quill::get_logger(), "Number in queue: {}",
				  call_in_queue.call.number);
		LOG_DEBUG(quill::get_logger(), "Number from request: {}", number);
		LOG_DEBUG(quill::get_logger(), "Comparison result: {}",
				  call_in_queue.call.number == number);
		if (call_in_queue.call.number == number)
		{
			inQueue = true;
			break;
		}
	}

	if (inQueue)
	{
		LOG_INFO(quill::get_logger(), "Number already in queue");
		rep.status = reply::status_type::internal_server_error;
		rep		   = reply::reply_from_code_and_content(
			   reply::status_type::internal_server_error,
			   "<html>"
				   "<head><title>Already In Queue</title></head>"
				   "<body><h1>500 Already in queue</h1></body>"
				   "</html>");
		return;
	}

	std::random_device rd;
	auto seed_data = std::array<int, std::mt19937::state_size>{};
	std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
	std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
	std::mt19937 generator(seq);
	uuids::uuid_random_generator gen{generator};

	uuids::uuid callId = gen();

	std::uniform_int_distribution<> dist(rmin_, rmax_);

	CallInfo call = CallInfo{
		.call		   = CallRecord{.creation_time =
								std::chrono::high_resolution_clock::now(),
									.id		= callId,
									.number = number},
		.timeout_point = (std::chrono::high_resolution_clock::now() +
						  std::chrono::seconds(dist(rd)))};


	if (queue_.size() >= capacity_)
	{
		LOG_INFO(quill::get_logger(), "Overflow");
		rep.status = reply::status_type::internal_server_error;
		rep		   = reply::reply_from_code_and_content(
			   reply::status_type::internal_server_error,
			   "<html>"
				   "<head><title>Overload</title></head>"
				   "<body><h1>500 Overload</h1></body>"
				   "</html>");

		call.call.end_time = std::chrono::high_resolution_clock::now();
		call.call.status   = "overload";
		call.call.write_cdr();
		return;
	}

	this->queue_.push_back(call);
	std::string rep_content = "";
	rep_content += "{\"CallID\":\"";
	rep_content += uuids::to_string(callId);
	rep_content += "\"}";

	rep.status	= reply::ok;
	rep.content = rep_content;
	rep.headers.resize(2);
	rep.headers[0].name	 = "Content-Length";
	rep.headers[0].value = std::to_string(rep.content.size());
	rep.headers[1].name	 = "Content-Type";
	rep.headers[1].value = "application/json";
}
