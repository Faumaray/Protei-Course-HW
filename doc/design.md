# Design Document


## Used technologies

Project is built on these technologies:

* Git
    - Source control
* CMake
    - Used as build tool
    - Doxygen generation
    - CTest - for running tests
    - CPack - for packaging the releases
        + NSIS - windows installer
        + RPM / DEB - linux packages
        + compressed archives
* Doxygen
    - For documentation and API reference generation
    - Generates wiki from markdown files
    - Custom modern style included
        + to try modern style see doc/doxygen/Doxyfile.in line 3
        + if you do not care feel free to delete `doc/doxygen/*.html` and `doc/doxygen/*.ccs`
    - Graphviz - for creating UML diagrams with doxygen
* clang-format 
    - Configuration file in the root of project for easy formating
    - Clang-format automatically uses nearest *.clang-format* file
    - Target *format* will run format on all source files
* clang-tidy 
    - With target *tidy* you can run static code analysis
* cppcheck
    - With target *cppcheck* you can run static code analysis
* [Glaze](https://github.com/stephenberry/glaze)
    - Extremely fast, in memory, JSON and interface library for modern C++
    - Downloaded via CPM.cmake from *external/CMakeLists.txt*
* [Quill](https://github.com/odygrd/quill)
    - Asynchronous Low Latency C++ Logging Library
    - Downloaded via CPM.cmake from *external/CMakeLists.txt*
* [GTest](https://github.com/google/googletest)
    - Testing and mocking framework made by Google
    - Downloaded via CPM.cmake from *external/CMakeLists.txt*
* [ASIO](https://github.com/chriskohlhoff/asio)
    - Cross-platform C++ library for network and low-level I/O programming (Standalone version)
    - Downloaded via CPM.cmake from *external/CMakeLists.txt*


## Deployment view
[//]: <> (TODO: add plantUML how this all works.)
