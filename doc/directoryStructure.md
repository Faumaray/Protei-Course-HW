


# Directory structure

* mainProjectFolder
    - **build** - user created, build takes place here
        + **dist** - here builded application is copied (by deafult, see CMAKE_INSTALL_PREFIX in main CMakeLists.txt)
            * **bin**
            * **share**
                - **data** - here folder data is copied
            * **doc**
                - doxygen lives here documentation
    - **cmake** - contains cmake scripts that can be included with include()
        + cleanCppExtensions.cmake - reusable helpers for source/CMakeLists.txt
        + sanitizers.cmake - adds sanitizers options
        + CPM.cmake - adds CPM(dependency manager for cmake)
        + useAltLinker.cmake - enables simple use of gold or mold linker for faster linkage
    - **data** - contain data
    - **doc** - contains documentation
        + CMakeLists.txt - doc building
        + **doxygen**
            * doxygen settings, and modern style config
    - **packaging**
        + CMakeLists.txt - package creation
        + ProcessingCenter.desktop - linux shortcut (app is then visible in launchers)
        + ProcessingCenter.png - icon for linux shortcut
        + ProcessingCenter.icon.in.rc - icon for windows description file (used in source/CMakeLists.txt)
        + ProcessingCenter.ico - icon for windows shortcut (linked via ProcessingCenter.icon.in.rc into the executable)
    - **include** - contain header files
        + operator - sources for operator program(emulation)
        + processingCenter - sources for ProcessingCenter program(server)
    - **source** - contain source files
        + CMakeLists.txt - source building
        + operator - sources for operator program(emulation)
        + processingCenter - sources for ProcessingCenter program(server)
        + **unittest* contains unittest build
            + CMakeLists.txt - unittest building
            + testmain.cpp - main unit tests function
    - **test** - integration tests, CTest, data sets for tests and unit tests
        + CMakeLists.txt - tests specification
    - **external**
        + CMakeLists.txt - external projects handling
    - readme.md - main readme file
    - CMakeLists.md - Main CMake configuration
    - .clang-format - clang format config file
    - .clang-tidy - clang tidy config file
    - .gitlab-ci.yml - continuous integration configuration for Gitlab CI


