#pragma once
#include <agrpc/grpc_executor.hpp>
#include <asio/signal_set.hpp>
#include <grpcpp/server.h>

#include <atomic>
#include <thread>

struct ServerShutdown
{
	grpc::Server& server;
	asio::basic_signal_set<agrpc::GrpcExecutor> signals;
	std::atomic_bool is_shutdown{};
	std::thread shutdown_thread;

	ServerShutdown(grpc::Server& server, agrpc::GrpcContext& grpc_context)
		: server(server)
		, signals(grpc_context, SIGINT, SIGTERM)
	{
		signals.async_wait(
			[&](auto&& ec, auto&&)
			{
				if (asio::error::operation_aborted != ec)
				{
					shutdown();
				}
			});
	}

	void shutdown()
	{
		if (!is_shutdown.exchange(true))
		{
			shutdown_thread = std::thread(
				[&]
				{
					signals.cancel();
					server.Shutdown();
				});
		}
	}

	~ServerShutdown()
	{
		if (shutdown_thread.joinable())
		{
			shutdown_thread.join();
		}
		else if (!is_shutdown.exchange(true))
		{
			server.Shutdown();
		}
	}
};
