#pragma once

#include <algorithm>
#include <asio/io_context.hpp>
#include <quill/Quill.h>
#include <quill/Utility.h>
#include <quill/detail/LogMacros.h>



void run_io_context(asio::io_context& io_context)
{
	try
	{
		io_context.run();
	}
	catch (const std::exception& e)
	{
		LOG_CRITICAL(quill::get_logger(), "Cannot run context: {}", e.what());
		std::abort();
	}
}


template <class OnExit>
struct ScopeGuard
{
	OnExit on_exit;

	explicit ScopeGuard(OnExit on_exit)
		: on_exit(std::move(on_exit))
	{
	}

	~ScopeGuard()
	{
		on_exit();
	}
};
