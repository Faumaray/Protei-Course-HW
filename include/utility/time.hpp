#pragma once
#include <chrono>
#include <ctime>
#include <format>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

template <typename Clock, typename Duration>
std::string
formatTimePoint(const std::chrono::time_point<Clock, Duration>& timePoint)
{
	auto time_t_point = std::chrono::system_clock::to_time_t(timePoint);
	std::tm tm		  = *std::localtime(&time_t_point);

	// Format the time point
	std::ostringstream oss;
	oss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S");

	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(
							timePoint.time_since_epoch()) %
		1000;
	oss << '.' << std::setw(3) << std::setfill('0') << milliseconds.count();

	return oss.str();
}

template <typename Clock, typename Duration>
std::chrono::time_point<Clock, Duration>
parseTimePoint(const std::string& timePointString)
{
	std::tm tm = {};
	std::istringstream iss(timePointString);

	// Parse the date and time part
	iss >> std::get_time(&tm, "%Y-%m-%d %H:%M:%S");
	auto sys_time = std::chrono::system_clock::from_time_t(std::mktime(&tm));

	// Parse the milliseconds part
	std::string millisecondsStr;
	iss >> millisecondsStr;
	if (!millisecondsStr.empty() && millisecondsStr.front() == '.')
	{
		millisecondsStr.erase(millisecondsStr.begin()); // Remove the dot
		auto milliseconds = std::stoi(millisecondsStr);
		sys_time += std::chrono::milliseconds(milliseconds);
	}

	return std::chrono::time_point_cast<Duration>(sys_time);
}
// Convert duration to string
template <typename Rep, typename Period>
std::string formatDuration(const std::chrono::duration<Rep, Period>& duration)
{
	auto hours	 = std::chrono::duration_cast<std::chrono::hours>(duration);
	auto minutes = std::chrono::duration_cast<std::chrono::minutes>(
		duration % std::chrono::hours(1));
	auto seconds = std::chrono::duration_cast<std::chrono::seconds>(
		duration % std::chrono::minutes(1));
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(
		duration % std::chrono::milliseconds(1000));

	std::ostringstream oss;
	oss << std::setfill('0') << std::setw(2) << hours.count() << ":"
		<< std::setw(2) << minutes.count() << ":" << std::setw(2)
		<< std::setw(2) << seconds.count() << "." << std::setw(3)
		<< milliseconds.count() << std::setw(3);
	return oss.str();
}

// Convert string to duration
template <typename Rep, typename Period>
std::chrono::duration<Rep, Period> parseDuration(const std::string& str)
{
	std::chrono::duration<Rep, Period> duration;

	std::istringstream iss(str);
	char delimiter;
	Rep hours, minutes, seconds, milliseconds;

	iss >> hours >> std::noskipws >> delimiter >> minutes >> std::noskipws >>
		delimiter >> seconds >> delimiter >> milliseconds;

	duration += std::chrono::hours(hours);
	duration += std::chrono::minutes(minutes);
	duration += std::chrono::seconds(seconds);
	duration += std::chrono::milliseconds(milliseconds);

	return duration;
}
