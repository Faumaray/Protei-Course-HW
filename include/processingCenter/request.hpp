#pragma once
#include "header.hpp"
#include <string>
#include <vector>

struct request
{
	std::string method;

	std::string uri;

	int http_version_major;

	int http_version_minor;

	std::vector<header> headers;

	std::string content;
};
