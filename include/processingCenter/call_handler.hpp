#pragma once
#include "call_record.hpp"
#include "reply.hpp"
#include "request.hpp"
#include "service.hpp"
#include <chrono>
#include <deque>
#include <memory>
#include <shared_mutex>
#include <tuple>
#include <uuid.h>
struct CallInfo
{
	CallRecord call;
	std::chrono::time_point<std::chrono::high_resolution_clock> timeout_point;
};

class CallHandler
{
  public:
	explicit CallHandler(std::shared_ptr<ServerServiceImpl> service,
						 std::size_t capacity,
						 std::uint_fast32_t rmin, std::uint_fast32_t rmax);

	void operator()(const request& req, reply& rep);
  private:
	std::size_t capacity_;
    std::shared_ptr<ServerServiceImpl> service_;
	std::deque<CallInfo> queue_;
	std::uint_fast32_t rmin_;
	std::uint_fast32_t rmax_;
    bool checker_started_ = false;

    void start_checker();
};
