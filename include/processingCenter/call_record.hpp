#pragma once

#include "time.hpp"
#include <chrono>
#include <fstream>
#include <ios>
#include <string>
#include <uuid.h>


class CallRecord
{
  public:
	std::chrono::time_point<std::chrono::high_resolution_clock> creation_time;
	uuids::uuid id;
	std::string number;
	std::chrono::time_point<std::chrono::high_resolution_clock> end_time;
	std::string status; // TODO: find better way to represent status
	std::optional<std::chrono::time_point<std::chrono::high_resolution_clock>>
		operator_answer_time;
	std::optional<uuids::uuid> operator_id;

	void write_cdr()
	{
		std::ofstream cdr_file;
		auto now				 = std::chrono::system_clock::now();
		std::time_t currentTime	 = std::chrono::system_clock::to_time_t(now);
		struct std::tm* timeInfo = std::localtime(&currentTime);
		std::ostringstream oss;
		oss << std::put_time(timeInfo, "%Y-%m-%d");
		cdr_file.open(std::format("{}.cdr", oss.str()),
					  std::ios_base::app | std::ios_base::out);
		cdr_file << formatTimePoint(creation_time) << ";";
		cdr_file << uuids::to_string(id) << ";";
		cdr_file << number << ";";
		cdr_file << formatTimePoint(end_time) << ";";
		cdr_file << status << ";";

		if (operator_answer_time.has_value())
		{
			cdr_file << operator_answer_time.value() << ";";
		}
		else
		{
			cdr_file << ";";
		}

		if (operator_id.has_value())
		{
			cdr_file << uuids::to_string(operator_id.value()) << ";";
		}
		else
		{

			cdr_file << ";";
		}

		if (operator_answer_time.has_value())
		{
			cdr_file
				<< formatDuration(
					   std::chrono::duration_cast<std::chrono::milliseconds>(
						   operator_answer_time.value() - end_time))
				<< ";";
		}
		else
		{
			cdr_file << ";";
		}

		cdr_file << "\n";
	}
};
