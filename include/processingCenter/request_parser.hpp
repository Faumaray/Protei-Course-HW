#pragma once
#include "request.hpp"
#include <asio/coroutine.hpp>
#include <optional>
#include <string>
#include <tuple>
/// Parser for incoming requests.
class request_parser : asio::coroutine
{
  public:
	template <typename InputIterator>
	std::tuple<std::optional<bool>, InputIterator>
	parse(request& req, InputIterator begin, InputIterator end)
	{
		while (begin != end)
		{
			std::optional<bool> result = consume(req, *begin++);
			if (result.has_value() && (result || !result))
				return std::make_tuple(result, begin);
		}
		std::optional<bool> result{};
		return std::make_tuple(result, begin);
	}

  private:
	static std::string content_length_name_;

	std::size_t content_length_;

	std::optional<bool> consume(request& req, char input);

	static bool is_char(int c);

	static bool is_ctl(int c);

	static bool is_tspecial(int c);

	static bool is_digit(int c);

	static bool tolower_compare(char a, char b);

	bool headers_equal(const std::string& a, const std::string& b);
};
