#pragma once

#include "call_record.hpp"
#include "grpc/server/v1/server.grpc.pb.h"
#include <cstdint>
#include <map>
#include <uuid.h>
using grpc::server::v1::OperatorDestructionRequest;
using grpc::server::v1::OperatorInitializationRequest;
using grpc::server::v1::OperatorInitializationResponse;

class ServerServiceImpl final : public grpc::server::v1::Server::Service
{
  public:
	ServerServiceImpl()	 = default;
	~ServerServiceImpl() = default;
	grpc::Status
	OperatorInitialization(grpc::ServerContext* context,
						   const OperatorInitializationRequest* request,
						   OperatorInitializationResponse* response) override;

	grpc::Status
	OperatorDestruction(grpc::ServerContext* context,
						const OperatorDestructionRequest* request,
						google::protobuf::Empty* response) override;


	uuids::uuid get_first_free();

	void schedule_call(uuids::uuid operatorId, CallRecord& callInfo);

  private:
	std::string extractIpAddress(const std::string& uri);


	std::map<uuids::uuid, std::string> operators_ = {};
};
