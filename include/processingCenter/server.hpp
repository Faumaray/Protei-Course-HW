#pragma once
#include "call_handler.hpp"
#include "reply.hpp"
#include "request.hpp"
#include "request_parser.hpp"
#include <array>
#include <asio.hpp>
#include <functional>
#include <memory>
#include <optional>
#include <string>

class server : asio::coroutine
{
  public:
	explicit server(asio::io_context& io_context, const std::string& address,
					const std::string& port,
					std::shared_ptr<CallHandler> handler);

	void operator()(asio::error_code ec = asio::error_code(),
					std::size_t length	= 0);

  private:
	typedef asio::ip::tcp tcp;

	std::shared_ptr<CallHandler> request_handler_;

	std::shared_ptr<tcp::acceptor> acceptor_;

	std::shared_ptr<tcp::socket> socket_;

	std::shared_ptr<std::array<char, 8192>> buffer_;

	std::shared_ptr<request> request_;

	std::optional<bool> valid_request_;

	request_parser request_parser_;

	std::shared_ptr<reply> reply_;
};
