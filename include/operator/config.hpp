#pragma once

#include <cstdint>
#include <glaze/core/macros.hpp>
#include <glaze/glaze.hpp>
#include <quill/Quill.h>
#include <quill/detail/LogMacros.h>
#include <sstream>
#include <string>
#include <string_view>
struct Config
{
	std::uint_fast32_t rmin			= 2;
	std::uint_fast32_t rmax			= 100;
	std::string service_address		= "0.0.0.0";
	std::uint_fast32_t service_port = 50032;
	std::string server_address		= "localhost";
	std::uint_fast32_t server_port	= 50031;
	Config()						= default;
	Config(std::string_view file)
	{
		std::string buffer{};
		glz::read_file_json(*this, file, buffer);
		if (buffer.empty())
		{
			LOG_WARNING(
				quill::get_logger(),
				"Got empty config file or not found, using default parameters");
		}
		else
		{
			LOG_INFO(quill::get_logger(), "Got config from {}", file);
		}
	};
	friend std::ostream& operator<<(std::ostream& os, Config const& obj)
	{
		os << "{\n\t[rmin:rmax] : "
		   << "[" << obj.rmin << "," << obj.rmax << "]\n";
		os << "\tService info : " << obj.service_address << ":"
		   << obj.service_port << "\n";
		os << "\tServer info : " << obj.server_address << ":" << obj.server_port
		   << "\n}";
		return os;
	}
	friend struct fmtquill::formatter<Config>;
	GLZ_LOCAL_META(Config, rmin, rmax, service_address, service_port,
				   server_address, server_port);
};

template <>
struct fmtquill::formatter<Config> : ostream_formatter
{
};
template <>
struct quill::copy_loggable<Config> : std::true_type
{
};
