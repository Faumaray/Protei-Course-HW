#pragma once

#include "grpc/operatorservice/v1/operator.grpc.pb.h"
#include <uuid.h>
using grpc::operatorser::v1::OperatorService;
using grpc::operatorser::v1::OperatorStatusRequest;
using grpc::operatorser::v1::OperatorStatusResponse;
using grpc::operatorser::v1::CallRequest;
using grpc::operatorser::v1::CallResponse;

enum class Status
{
	Wait = 1,
	Busy = 2
};

class OperatorServiceImpl final : public OperatorService::Service
{
  public:
	OperatorServiceImpl(std::string server_address, std::uint_fast64_t rmin,
						std::uint_fast64_t rmax, std::string exchange);
	~OperatorServiceImpl();

	grpc::Status Check(grpc::ServerContext* context,
					   const OperatorStatusRequest* request,
					   OperatorStatusResponse* response) override;

	grpc::Status Call(grpc::ServerContext* context, const CallRequest* request,
					  CallResponse* response) override;

  private:
	uuids::uuid id_ = uuids::uuid();
	Status status_	= Status::Wait;
	std::chrono::duration<int> rmin_;
	std::chrono::duration<int> rmax_;
	std::string server_address_;
	std::string exchange_info_;
};
